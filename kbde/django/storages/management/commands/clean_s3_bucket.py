from kbde.django.management import loop
from kbde.django.storages import cleaner


class Command(loop.LoopCommand):
    
    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help=(
                "Don't delete anything. Print a message for each object "
                "instead."
            ),
        )

    def handle_loop(self, **options):
        cleaner_instance = cleaner.S3Cleaner(stdout=self.stdout)
        cleaner_instance.clean(dry_run=options["dry_run"])
