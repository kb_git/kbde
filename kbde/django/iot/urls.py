from django.urls import path, include


app_name = "iot"

urlpatterns = [
    path("api/", include("kbde.django.iot.api.urls")),
]
