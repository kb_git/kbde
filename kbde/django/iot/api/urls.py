from django.urls import path, include


app_name = "api"

urlpatterns = [
    path("v1/", include("kbde.django.iot.api.v1.urls")),
]
