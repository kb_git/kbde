from kbde.django import views as kbde_views
from kbde.django.json_views import views as json_views

from django_filters import views as filter_views


class FiltersetMixin(kbde_views.PostToGetMixin, filter_views.FilterMixin):

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)

        context_data["filterset"] = self.get_filterset()

        return context_data

    def get_filterset(self):

        if not hasattr(self, "filterset_cache"):
            filterset_class = self.get_filterset_class()
            self.filterset_cache = super().get_filterset(filterset_class)

        return self.filterset_cache

    def get_filterset_class(self):
        assert self.filterset_class is not None, (
            f"{self.__class__} must define .filterset_class"
        )
        return self.filterset_class

    def get_filterset_kwargs(self, *args, **kwargs):
        return {
            "data": self.request.GET or None,
            "request": self.request,
            "queryset": self.get_filter_queryset(),
        }

    def get_filter_queryset(self):
        return self.get_queryset()


class FilterQuerysetMixin(FiltersetMixin):
    
    def get_queryset(self):
        filterset = self.get_filterset()

        if (
            not filterset.is_bound
            or filterset.is_valid()
            or not self.get_strict()
        ):
            return filterset.qs
        else:
            return filterset.queryset.none()

    def get_filter_queryset(self):
        return super().get_queryset()


class JsonFiltersetMixin(FilterQuerysetMixin):
    
    def get_response_context(self, context):
        response_context = super().get_response_context(context)

        form = self.get_filterset().form

        if hasattr(form, "cleaned_data"):
            del form.cleaned_data

        response_context["filterset"] = form

        return response_context


class FilterFormView(FiltersetMixin, kbde_views.FormView, kbde_views.ListView):
    method = "GET"
    submit_button_text = "Apply Filters"

    def get_form(self):
        return self.get_filterset().form
