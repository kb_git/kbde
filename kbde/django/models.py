from django.db import models
from django.db.models import signals
from django.core import exceptions
from django.conf import settings
from django.utils import timezone
from django.template import loader
from django.contrib.auth import (
    models as auth_models,
    base_user,
)
from kbde.django import utils as kbde_utils
from kbde.django.rq import utils as rq_utils

import uuid, io, base64


MAX_LENGTH_CHAR_FIELD = 255


def get_upload_to(obj, name):
    return f"upload/{uuid.uuid4()}/{name}"


class Model(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4)
    time_created = models.DateTimeField(auto_now_add=True)
    time_updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(auth_models.AbstractUser, Model):

    class Meta:
        abstract = True


class EmailUserManager(base_user.BaseUserManager):

    def create_user(self, email, password=None):

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password):

        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user

    def get_by_natural_key(self, email):
        return self.get(email__iexact=email)


class EmailUser(User):
    email = models.CharField(max_length=MAX_LENGTH_CHAR_FIELD, unique=True)
    username = models.CharField(max_length=MAX_LENGTH_CHAR_FIELD, blank=True)

    objects = EmailUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    
    class Meta:
        abstract = True

    def __str__(self):
        return self.email

    def clean(self):
        super().clean()

        # Check to make sure that another user with this same email does not
        # already exist
        if (
            self.__class__.objects
            .exclude(pk=self.pk)
            .filter(email__iexact=self.email)
            .exists()
        ):
            raise exceptions.ValidationError({
                "email": "A user with this email already exists",
            })


class QrCodeModel(Model):
    QR_CODE_ERROR_LEVEL_L = "L"
    QR_CODE_ERROR_LEVEL_M = "M"
    QR_CODE_ERROR_LEVEL_Q = "Q"
    QR_CODE_ERROR_LEVEL_H = "H"
    QR_CODE_ERROR_LEVEL_CHOICES = (
        (QR_CODE_ERROR_LEVEL_L, "7%"),
        (QR_CODE_ERROR_LEVEL_M, "15%"),
        (QR_CODE_ERROR_LEVEL_Q, "25%"),
        (QR_CODE_ERROR_LEVEL_H, "30%"),
    )
    qr_code_error_level = models.CharField(
        max_length=1,
        choices=QR_CODE_ERROR_LEVEL_CHOICES,
        default=QR_CODE_ERROR_LEVEL_L,
    )
    
    class Meta:
        abstract = True

    def get_qr_code_src(self):
        """
        Produces the QR code for this LinkGroup.
        Formats the QR code as bytes for use in an <img> tag.
        """
        bytestring = self.get_qr_code_bytestring()

        return f"data:image/svg+xml;base64,{bytestring}"

    def get_qr_code_bytestring(self):
        """
        Returns the QR code as a base64-encoded bytestring
        """
        qr_code_svg = self.get_qr_code_svg()

        qr_code_bytes = qr_code_svg.getvalue()
        
        return base64.b64encode(qr_code_bytes).decode()

    def get_qr_code_svg(self, scale=6):
        """
        Produces the QR code for this LinkGroup in the SVG format.
        Returns in-memory file object.
        """
        buf = io.BytesIO()

        qr_code = self.get_qr_code()
        qr_code.svg(buf, scale=scale)

        return buf

    def get_qr_code(self):
        import pyqrcode

        return pyqrcode.create(
            self.get_qr_code_data(),
            error=self.qr_code_error_level,
        )

    def get_qr_code_data(self):
        raise NotImplementedError(
            f"{self.__class__} must implement .get_qr_code_src()"
        )


class SendEmailModel(Model):
    email_template_name = None
    email_subject = None

    time_sent = models.DateTimeField(null=True, editable=False)
    
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        new = not self.pk

        super().save(*args, **kwargs)

        if new:
            self.send_email()

    def send_email(self):
        kbde_utils.send_email(**self.get_send_email_kwargs())
        self.time_sent = timezone.now()
        super().save(update_fields=["time_sent"])

    def get_send_email_kwargs(self):
        html = loader.render_to_string(
            template_name=self.get_email_template_name(),
            context=self.get_email_context_data(),
        )
            
        email_list = self.get_email_list()
        assert isinstance(email_list, (list, tuple)), (
            f"{self.__class__} .get_email_list() must return a list or tuple"
        )

        return {
            "to_email_list": email_list,
            "subject": self.get_email_subject(),
            "html_message": html,
        }

    def get_email_template_name(self):
        
        if self.email_template_name is not None:
            return self.email_template_name

        path_name = self.__class__.__name__

        # Template name is {module_name}/{path_name}.html
        module_name_list = self.__class__.__module__.split(".")

        module_name = "/".join(module_name_list)
        template_name = f"{module_name}/{path_name}.html"

        return template_name

    def get_email_context_data(self):
        return {
            "object": self,
        }

    def get_email_list(self):
        """
        Return a list or tuple of email addresses to send to
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .get_email_list()"
        )

    def get_email_subject(self):
        assert self.email_subject is not None, (
            f"{self.__class__} must define .email_subject or "
            f"override .get_email_subject()"
        )
        return self.email_subject


class CalculatedModel(Model):
    """
    This model defines a set of `.calculated_fields`. For each calculated
    field, a function is called on `save()` which calculates the a value and
    saves it into a model field.
    """
    calculated_model_queue_name = settings.CALCULATED_MODEL_QUEUE_NAME
    calculate_on_save = True
    calculated_fields = None

    class Meta:
        abstract = True

    def schedule_recalculate(self, *args, **kwargs):
        return rq_utils.queue_job(
            self.get_calculated_model_queue_name(),
            self.run_recalculate,
            *args,
            **kwargs,
        )

    def run_recalculate(self, *args, **kwargs):
        return self.recalculate(*args, **kwargs)

    def schedule_save(self, *args, **kwargs):
        return rq_utils.queue_job(
            self.get_calculated_model_queue_name(),
            self.run_save,
            *args,
            **kwargs,
        )

    def run_save(self, *args, **kwargs):
        return self.save(*args, **kwargs)

    def recalculate(self, fields=None, refresh_from_db=True):

        if refresh_from_db:
            self.refresh_from_db()
        
        if fields is None:
            fields = self.get_calculated_fields()

        self.calculate_fields(fields=fields)
        self.save(calculate_fields=False, update_fields=fields)

    def save(self, calculate_fields=True, *args, **kwargs):
        
        if self.calculate_on_save and calculate_fields:
            self.calculate_fields()

        super().save(*args, **kwargs)

    def calculate_fields(self, fields=None):

        if fields is None:
            fields = self.get_calculated_fields()

        for calculated_field in fields:
            value = self.get_calculated_value(calculated_field)
            setattr(self, calculated_field, value)

    def get_calculated_fields(self):
        assert self.calculated_fields is not None, (
            f"{self.__class__} must define .calculated_fields"
        )
        return self.calculated_fields.copy()

    def get_calculated_value(self, calculated_field):
        function = self.get_calculate_function(calculated_field)
        return function()

    def get_calculate_function(self, calculated_field):
        function_name = self.get_calculate_function_name(calculated_field)
        return getattr(self, function_name)

    def get_calculate_function_name(self, calculated_field):
        return f"calculate_{calculated_field}"

    def get_calculated_model_queue_name(self):
        return self.calculated_model_queue_name


class SignalListenerModel(Model):
    pre_save_senders = []
    post_save_senders = []
    pre_delete_senders = []
    post_delete_senders = []
    
    class Meta:
        abstract = True

    @classmethod
    def set_signals(cls):
        assert not hasattr(cls, "signals_set"), (
            f"Signals have already been set for {cls}"
        )

        cls.set_pre_save_signals()
        cls.set_post_save_signals()
        cls.set_pre_delete_signals()
        cls.set_post_delete_signals()

        cls.signals_set = True

    @classmethod
    def set_pre_save_signals(cls):
        
        for sender in cls.get_pre_save_senders():
            cls.connect_signal("pre_save", signals.pre_save, sender)

    @classmethod
    def set_post_save_signals(cls):
        
        for sender in cls.get_post_save_senders():
            cls.connect_signal("post_save", signals.post_save, sender)

    @classmethod
    def set_pre_delete_signals(cls):
        
        for sender in cls.get_pre_delete_senders():
            cls.connect_signal("pre_delete", signals.pre_delete, sender)

    @classmethod
    def set_post_delete_signals(cls):
        
        for sender in cls.get_post_delete_senders():
            cls.connect_signal("post_delete", signals.post_delete, sender)

    @classmethod
    def connect_signal(cls, prefix, signal, sender):
        model_name = sender._meta.model_name
        callback_function_name = f"{prefix}_{model_name}"
        callback_function = getattr(cls, callback_function_name)
        signal.connect(callback_function, sender=sender)

    @classmethod
    def get_pre_save_senders(cls):
        return cls.pre_save_senders.copy()

    @classmethod
    def get_post_save_senders(cls):
        return cls.post_save_senders.copy()

    @classmethod
    def get_pre_delete_senders(cls):
        return cls.pre_delete_senders.copy()

    @classmethod
    def get_post_delete_senders(cls):
        return cls.post_delete_senders.copy()
