from django import template, urls, http
from django.core import exceptions
from kbde.import_tools import utils as import_utils


register = template.Library()


def get_view_by_url(request, url):

    try:
        # Try resolving the url
        url = url.split("?")[0]
        resolver_match = urls.resolve(url)
        view_class = resolver_match.func.view_class
        view = view_class()
        view.setup(request, *resolver_match.args, **resolver_match.kwargs)
        return view

    except urls.exceptions.Resolver404:
        return None


def get_view_by_path(request, path):

    try:
        # Try importing the url by the view name
        view_class = import_utils.import_from_string(path)
        view = view_class()
        view.setup(request)
        return view

    except import_utils.ImportUtilsException:
        return None


@register.filter()
def has_permissions(request, view_location):
    """
    Takes a request and view_location.
    The view_location can be a full url, or a path to a view.
    If a URL can not be resolved from view_location, then the view location
    will be used to import the view_class
    """
    view = (
        get_view_by_url(request, view_location)
        or get_view_by_path(request, view_location)
    )

    assert view is not None, (
        f"Could not find view for \"{view_location}\". This must be either a "
        f"resolvable url, or a path to a view (i.e. my_module.MyViewClass)"
    )

    try:
        view.check_permissions(soft_check=True)
        return True

    except exceptions.PermissionDenied:
        return False

    except http.Http404:
        assert False, (
            f"Permission check raised http.Http404 for: {view_location}"
        )
