from django import http
from kbde.django import views as kbde_views
import json


class DropzoneJsMixin:
    template_name = "kbde/django/dropzone/views/DropzoneJsForm.html"
    dropzone_config = {}

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data.update({
            "dropzone_config": self.get_dropzone_config_json(),
        })

        return data

    def get_dropzone_config_json(self):
        return json.dumps(self.get_dropzone_config())

    def get_dropzone_config(self):
        return self.dropzone_config.copy()

    def get_action(self):
        action = super().get_action()
        assert action, (
            f"{self.__class__} must define .action or override the "
            f".get_action() method"
        )

        return action

    def form_valid(self, form):
        super().form_valid(form)
        return self.get_success_response()

    def get_success_url(self):
        return None

    def get_success_response(self):
        return http.HttpResponse(status=201)

    def form_invalid(self, form):
        response_data = self.get_form_invalid_response_data(form)
        response = http.HttpResponse(
            json.dumps(response_data),
            status=422,
        )
        response["content-type"] = "application/json"
        return response

    def get_form_invalid_response_data(self, form):
        error_list = []

        for field, errors in form.errors.items():
            
            for error in errors:
                error_list.append(error)
                
        errors = ", ".join(error_list)
        return {"error": errors}