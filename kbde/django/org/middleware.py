from django.utils import functional

from . import models


class OrgUserMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.org_user = functional.SimpleLazyObject(
            lambda: self.get_org_user(request)
        )
        return self.get_response(request)

    def get_org_user(self, request):
        
        if not request.user.is_authenticated:
            return None

        org_user = self.get_org_user_from_session(request)

        if org_user is None:
            org_user = self.get_default_org_user(request)

        return org_user

    def get_org_user_from_session(self, request):
        org_user_pk = request.session.get("org_user_pk")

        if not org_user_pk:
            return None

        try:
            return models.OrgUser.objects.get(pk=org_user_pk)
        except models.OrgUser.DoesNotExist:
            return None

    def get_default_org_user(self, request):
        default_org_user = getattr(request.user, "default_org_user", None)
        
        if default_org_user is not None:
            return default_org_user

        return request.user.orguser_set.order_by("pk").first()
