from django.apps import AppConfig


class PhoneNumberVerificationConfig(AppConfig):
    name = 'kbde.django.verification.phone_number'
    label = 'kbde_django_verification_phone_number'
    default_auto_field = 'django.db.models.BigAutoField'
