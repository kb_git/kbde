from kbde.django import views as kbde_views

from . import forms, models


class PhoneNumberVerificationCreate(kbde_views.CreateView):
    model = models.PhoneNumberVerification
    form_class = forms.PhoneNumberVerificationCreate


class PhoneNumberVerificationLoginCreate(VerificationLoginCreateMixin,
                                         PhoneNumberVerificationCreate):
    prompt_text = "Enter your phone number to log in"
