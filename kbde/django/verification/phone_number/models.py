from django.db import models
from django.conf import settings
from django.contrib import auth
from phonenumber_field import modelfields as phonenumber_models
from kbde.django import models as kbde_models

from .. import models as verification_models


class PhoneNumberVerification(verification_models.Verification):
    phone_number = phonenumber_models.PhoneNumberField()
    message_template = getattr(
        settings,
        "PHONE_NUMBER_VERIFICATION_MESSAGE",
        "Your verification code is: {raw_key}",
    )

    def send(self, raw_key):
        message = (
            self.get_message_template()
            .format(**self.get_message_kwargs(raw_key=raw_key))
        )
        self.send_sms(message)

    def get_message_template(self):
        return self.message_template

    def get_message_kwargs(self, **kwargs):
        return kwargs

    def send_sms(self, message):
        raise NotImplementedError(
            f"{self.__class__} must implement .send_sms()"
        )

    def get_user(self):
        User = auth.get_user_model()
        return User.objects.get(phone_number=self.phone_number)
