from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib import auth
from django.contrib.auth import hashers
from django.template import loader
from kbde.django import (
    models as kbde_models,
    utils as kbde_utils,
)

import random, datetime


class Verification(kbde_models.Model):
    key_allowed_characters = "0123456789"
    key_length = models.PositiveIntegerField(default=6)
    key = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        blank=True,
    )

    is_sent = models.BooleanField(default=False)
    time_sent = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    time_completed = models.DateTimeField(null=True, blank=True)

    time_valid_seconds = models.PositiveIntegerField(
        blank=True,
        default=getattr(
            settings,
            "VERIFICATION_DEFAULT_TIME_VALID",
            10 * 60,  # 10 minutes
        )
    )
    expire_time = models.DateTimeField(null=True, blank=True)

    child_attrs = getattr(
        "settings",
        "VERIFICATION_CHILD_ATTRS",
        [
            "emailverification",
            "phonenumberverification",
        ],
    )

    @classmethod
    def get_user_update_queryset(cls, user):
        return cls.objects.filter(is_completed=False)

    def save(self, *args, **kwargs):

        if self.expire_time is None and self.time_valid_seconds is not None:
            self.expire_time = (
                timezone.now()
                + datetime.timedelta(seconds=self.time_valid_seconds)
            )

        raw_key = None

        if not self.key:
            raw_key = self.generate_key()
            self.key = hashers.make_password(raw_key)

        if raw_key and not self.is_sent:
            self.is_completed = False
            self.send(raw_key)
            self.is_sent = True
            self.time_sent = timezone.now()

        if self.is_completed:
            self.time_completed = self.time_completed or timezone.now()
        else:
            self.time_completed = None

        return super().save(*args, **kwargs)
        
    def generate_key(self):
        """
        By default, this just returns a random string made of characters found
        in self.key_allowed_characters, and a length of self.key_length
        """
        return "".join(
            random.choice(self.key_allowed_characters).upper()
            for i in range(self.key_length)
        )

    def send(self, raw_key):
        """
        Send the raw key so the user can verify that they received it
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .send()"
        )

    def verify(self, raw_key):

        if self.expire_time is not None and self.expire_time <= timezone.now():
            raise self.VerificationExpired

        if not hashers.check_password(raw_key.upper(), self.key):
            raise self.IncorrectKey

        self.is_completed = True
        self.save()

    def get_user(self):
        """
        Gets the user related to whatever is being verified, i.e., email,
        phone number, etc
        """
        return self.get_child().get_user()

    def get_child(self):
        children = (getattr(self, child_attr, None) for child_attr in self.get_child_attrs())
        return next(child for child in children if child is not None)

    def get_child_attrs(self):
        return self.child_attrs.copy()

    class VerificationException(Exception):
        pass

    class VerificationFailed(VerificationException):
        pass

    class VerificationExpired(VerificationFailed):
        pass

    class IncorrectKey(VerificationFailed):
        pass


class EmailVerification(Verification):
    email = models.EmailField()
    subject = models.CharField(
        max_length=kbde_models.MAX_LENGTH_CHAR_FIELD,
        default=getattr(
            settings,
            "VERIFICATION_EMAIL_DEFAULT_SUBJECT",
            "Verification Code"
        ),
    )
    from_email = models.EmailField(blank=True)
    email_template_name = getattr(
        settings,
        "VERIFICATION_EMAIL_TEMPLATE_NAME",
        "kbde/django/verification/verification_email.html",
    )

    def send(self, raw_key):
        email_context = {
            "object": self,
            "raw_key": raw_key,
        }
        email_template_name = self.get_email_template_name()
        email_html = loader.render_to_string(
            email_template_name,
            email_context,
        )

        kbde_utils.send_email(
            [self.email],
            subject=self.subject,
            html_message=email_html,
            from_email=self.from_email or None,
        )

    def get_email_template_name(self):
        return self.email_template_name

    def get_user(self):
        User = auth.get_user_model()
        return User.objects.get(email__iexact=self.email)
