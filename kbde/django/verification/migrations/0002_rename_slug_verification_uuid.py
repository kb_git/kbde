# Generated by Django 3.2.12 on 2024-03-05 16:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kbde_django_verification', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='verification',
            old_name='slug',
            new_name='uuid',
        ),
    ]
