from django import forms
from django.contrib import auth

from . import models


class VerificationVerify(forms.Form):
    key = forms.CharField(label="Verification Code")
    
    def __init__(self, instance, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance = instance

    def clean(self):
        super().clean()
        
        if self.errors:
            return None

        # Check if the key can be verified
        try:
            self.instance.verify(self.cleaned_data["key"])
        except self.instance.IncorrectKey:
            self.add_error("key", "The code entered was incorrect")
        except self.instance.VerificationExpired:
            self.add_error("key", "This code is no longer valid")

    def save(self, *args, **kwargs):
        return self.instance


class EmailVerificationCreate(forms.ModelForm):
    
    class Meta:
        model = models.EmailVerification
        fields = [
            "email",
        ]

    def clean(self):
        super().clean()

        if self.errors:
            return None

        User = auth.get_user_model()

        # Try to find the user with this email
        try:
            User.objects.get(email__iexact=self.cleaned_data["email"])
        except User.DoesNotExist:
            self.add_error("email", "An account with this email was not found")
