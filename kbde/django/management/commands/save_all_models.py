from django.apps import apps
from django.core.management import base


class Command(base.BaseCommand):
    
    def add_arguments(self, parser):
        parser.add_argument("--clean", action="store_true")

    def handle(self, clean, **options):
        
        for model in apps.get_models():
            
            for obj in model.objects.iterator(chunk_size=1000):
                
                if clean:
                    obj.clean()

                obj.save()
