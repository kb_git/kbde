from django.db import models
from django.conf import settings
from django.utils import timezone
from django.template import loader
from kbde.django import (
    models as kbde_models,
    utils as kbde_utils,
)


class Contact(kbde_models.Model):
    is_complete = models.BooleanField(default=False)
    
    @classmethod
    def get_user_read_queryset(cls, user):
        return cls.objects.all()


class BaseMessage(kbde_models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    is_sent = models.BooleanField(default=False)
    time_sent = models.DateTimeField(blank=True, null=True)

    alert_from_email = getattr(settings, "CONTACT_ALERT_FROM_EMAIL", None)
    alert_email_subject = getattr(
        settings,
        "CONTACT_ALERT_EMAIL_SUBJECT",
        "New Contact Form",
    )
    alert_email_recipients = getattr(settings, "CONTACT_ALERT_EMAIL_RECIPIENTS", None)
    template_name = None
    
    class Meta:
        abstract = True

    @classmethod
    def get_user_read_queryset(cls, user):
        return cls.objects.all()

    def save(self, *args, **kwargs):

        if self.is_sent and self.time_sent is None:
            self.send()
            self.time_sent = timezone.now()
        
        if not self.is_sent:
            self.time_sent = None

        super().save(*args, **kwargs)

    def send(self, *args, **kwargs):
        app_host = getattr(settings, "APP_HOST", None)
        assert app_host, (
            "Must define APP_HOST in settings.py, which is the domain "
            "where the app is hosted, starting with http(s)://"
        )

        template_name = self.get_template_name()
        context = {
            "object": self,
            "APP_HOST": app_host,
        }
        html_message = loader.render_to_string(
            template_name,
            context,
        )
        recipients = self.get_recipients()
        kbde_utils.send_email(
            recipients,
            self.alert_email_subject,
            from_email=self.alert_from_email,
            html_message=html_message,
        )

    def get_recipients(self):
        assert self.alert_email_recipients is not None, (
            f"You must set the CONTACT_ALERT_EMAIL_RECIPIENTS setting, as a "
            f"list of email addresses"
        )
        return self.alert_email_recipients.copy()
        
    def get_template_name(self):
        assert self.template_name is not None, (
            f"{self.__class__} must define .template_name"
        )
        return self.template_name


class Message(BaseMessage):
    email = models.EmailField()
    message = models.TextField()

    template_name = "kbde/django/contact_us/models/Message.html"


class Voicemail(BaseMessage):
    message = models.FileField(upload_to=kbde_models.get_upload_to)

    template_name = "kbde/django/contact_us/models/Voicemail.html"
