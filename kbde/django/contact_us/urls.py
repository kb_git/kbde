from . import views


app_name = "kbde_django_contact_us"

urlpatterns = [
    views.ContactCreate.get_urls_path("contact/create"),
    views.ContactDetail.get_urls_path("contact/<uuid:slug>"),
    views.MessageCreate.get_urls_path("contact/<uuid:related_slug>/message/create"),
    views.VoicemailCreate.get_urls_path("contact/<uuid:related_slug>/voicemail/create"),
    views.ContactSuccess.get_urls_path("contact/<uuid:slug>/success"),
    views.MessageDetail.get_urls_path("message/<uuid:slug>"),
    views.VoicemailDetail.get_urls_path("voicemail/<uuid:slug>"),
]
