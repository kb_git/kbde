from django import http


class HttpClientMixin:
    http_client_class = None

    def get_object(self):
        obj = self.kwargs.get("object")
        
        if obj is not None:
            return obj

        client = self.get_http_client()
        response = client.get()
        return self.get_object_from_response(response)

    def get_object_from_response(self, response):
        raise NotImplementedError(
            f"{self.__class__} must implement .get_object_from_response()"
        )

    def get_queryset(self):
        object_list = self.kwargs.get("object_list")

        if object_list is not None:
            return object_list

        client = self.get_http_client()
        response = client.get()
        return self.get_queryset_from_response(response)

    def get_queryset_from_response(self, response):
        raise NotImplementedError(
            f"{self.__class__} must implement .get_queryset_from_response()"
        )
    
    def form_valid(self, form):
        success_url = self.get_success_url()

        client = self.get_http_client()
        self.object = client.post(**form.cleaned_data)

        return http.HttpResponseRedirect(success_url)

    def delete(self, *args, **kwargs):
        success_url = self.get_success_url()
        
        client = self.get_http_client()
        client.delete()
        
        return http.HttpResponseRedirect(success_url)

    def get_http_client(self):
        http_client_class = self.get_http_client_class()
        http_client_kwargs = self.get_http_client_kwargs()
        return http_client_class(**http_client_kwargs)

    def get_http_client_class(self):
        assert self.http_client_class is not None, (
            f"{self.__class__} must define .http_client_class"
        )
        return self.http_client_class

    def get_http_client_kwargs(self):
        return self.kwargs.copy()
