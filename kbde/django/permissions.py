from django import http
from django.conf import settings
from django.core import exceptions
from django.contrib import messages
from django.contrib.auth import mixins as auth_mixins


class Permission:
    """
    Base class for a permission
    """
    
    def __init__(self, view, soft_check):
        self.view = view
        self.soft_check = soft_check

    def check(self):
        """
        Should either:
        
        - Return None (no problems)
        - Return a response object (i.e. to perform a redirect)
        - Raise a permission denied exception

        If `self.soft_check == True`, then the permission should not alter any
        state, i.e. setting messages or altering data.
        """
        raise NotImplementedError(
            f"{self.__class__} must implement .check()"
        )


class IsAuthenticated(Permission, auth_mixins.LoginRequiredMixin):
    
    def check(self):
        """
        Make sure that the current user is authenticated
        Returns a login redirect if not
        """
        user = self.view.request.user

        if user and user.is_authenticated:
            return None
        
        # Set the request and let the LoginRequiredMixin handle the rest
        self.request = self.view.request
        return self.handle_no_permission()


class IsSuperuser(Permission):

    def check(self):

        if self.view.request.user.is_superuser:
            return None

        raise exceptions.PermissionDenied("Must be a superuser")


class IsStaff(Permission):

    def check(self):

        if self.view.request.user.is_staff:
            return None

        raise exceptions.PermissionDenied("Must be staff")


class DebugModeRequired(Permission):
    
    def check(self):

        if settings.DEBUG:
            return None

        raise exceptions.PermissionDenied("Must be run in debug mode")


class RedirectWithNext(Permission):
    redirect_url = None
    redirect_message = None
    redirect_message_level = messages.INFO

    def check(self):

        if self.test_func():
            return None

        # Test function failed
        # Redirect with next param
        redirect_url = self.get_redirect_url()
        url = f"{redirect_url}?next={self.view.get_current_url_safe()}"

        redirect_message = self.get_redirect_message()

        if not self.soft_check and redirect_message is not None:
            messages.add_message(
                self.view.request,
                self.get_redirect_message_level(),
                redirect_message,
            )

        return http.HttpResponseRedirect(url)
        
    def test_func(self):
        raise NotImplementedError(
            f"{self.__class__} must implement .test_func(). "
            f"This function should return True or False"
        )
    
    def get_redirect_url(self):
        assert self.redirect_url is not None, (
            f"{self.__class__} must define .redirect_url or override "
            f".get_redirect_url()"
        )
        return self.redirect_url

    def get_redirect_message(self):
        return self.redirect_message

    def get_redirect_message_level(self):
        return self.redirect_message_level


class CanGetObject(Permission):

    def check(self):
        """
        Check to make sure that the get_object method works
        """
        try:
            self.view.get_object()
        except http.Http404:
            raise exceptions.PermissionDenied(
                "User cannot get object"
            )


class CanGetRelatedObject(Permission):

    def check(self):
        """
        Check to make sure that the get_object method works
        """
        try:
            self.view.get_related_object()
        except http.Http404:
            raise exceptions.PermissionDenied(
                "User cannot get related_object"
            )


# TODO: Remove this
# https://trello.com/c/MuezQt45/58-remove-the-can-update-related-object-permission-check

class CanUpdateRelatedObject(Permission):
    
    def check(self):
        related_object = self.view.get_related_object()

        if not (
            self.view.related_model.get_user_update_queryset(self.view.request.user)
            .filter(pk=related_object.pk)
            .exists()
        ):
            raise exceptions.PermissionDenied(
                "User cannot update related object"
            )
