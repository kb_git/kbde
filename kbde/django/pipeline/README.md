# Pipeline

This module adds support for embedded Javascript and embedded Sass in webpages.

## How it works

When a response is passed through the middleware classes in this module, a few things happen:

1. All relevant embedded tags are collected by the middleware and stripped from the document:
  - `<script>` tags with no `src`.
  - `<style sass>` tags)
2. Content from those tags are deduplicated and processed:
  - JS is concatenated.
  - Sass is compliled to CSS.
3. Some content is placed back into the page:
  - Concatenated JS is added to one `<script>` tag at the bottom of the `<body>`.
  - CSS is stored in the cache, and a reference to it is placed into a `<link>` tag in the `<head>`.

## Configuring the cache

In your `settings.py` file:

```
CACHES = {
    # … default cache config and others
    "pipeline": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

PIPELINE_CACHE_NAME = "pipeline"  # This is the default value
```
