from django.core import cache
from django.conf import settings


DEFAULT_CACHE_NAME = "pipeline"


def get_pipeline_cache():
    pipeline_cache_name = getattr(
        settings,
        "PIPELINE_CACHE_NAME",
        DEFAULT_CACHE_NAME,
    )
    return cache.caches[pipeline_cache_name]


def get_cache_key(key):
    return f"pipeline:{key}"


def get_expire_cache_key(key):
    return f"pipeline:exp:{key}"
