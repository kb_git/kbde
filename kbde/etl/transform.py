

class Transformer:

    def transform(self, object_list):
        
        for obj in object_list:
            yield self.transform_object(obj)

    def transform_object(self, obj):
        raise NotImplementedError(
            f"{self.__class__} must implement .transform_object()"
        )


class KeyMapTransformer(Transformer):
    """
    Replace keys by a full text-match:

    key_map = {
        "old_key_1": "new_key_1",
        "old_key_2": "new_key_2",
        ...
    }

    Other keys are not modified
    """
    key_map = {}

    def transform_object(self, obj):
        return {
            self.key_map.get(key, key): value for key, value in obj.items()
        }


class KeyReplaceTransformer(Transformer):
    """
    Perform a text replace from self.old to self.new on all keys
    """
    old = None
    new = None

    def __init__(self, *args, **kwargs):
        assert self.old is not None and self.new is not None
        super().__init__(*args, **kwargs)

    def transform_object(self, obj):
        return {
            key.replace(self.old, self.new): value for key, value in obj.items()
        }


class KeyFilterTransformer(Transformer):
    """
    Only keep keys that are in self.key_filter
    Throw the others away
    """
    key_filter = []

    def transform_object(self, obj):
        assert self.key_filter
        return {
            key: value for key, value in obj.items() if key in self.key_filter
        }
