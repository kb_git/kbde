import importlib


class ImportUtilsException(Exception):
    pass


def import_from_string(import_string):
    split_import_string = import_string.split(".")
    module_string = ".".join(split_import_string[:-1])

    if not module_string:
        raise ImportUtilsException(
            f"The given string to import, {import_string}, did not contain a "
            f"module and an attribute, i.e. my_module.MyClass or my_module.my_function"
        )

    module = importlib.import_module(module_string)

    return getattr(module, split_import_string[-1])
