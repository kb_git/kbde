from . import client_base


class Account(client_base.Base):
    path = "/account"
    response_status_codes = [
        200,
    ]
