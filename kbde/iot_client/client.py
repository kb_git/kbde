from kbde.http_client import client as http_client

import time, threading, os, traceback, sys


class BaseClient(http_client.HttpClient):
    headers = {
        "authorization": "Token {api_key}",
    }

    def __init__(self, host, **kwargs):
        self.host = host
        super().__init__(**kwargs)


# Device

class DeviceDetail(BaseClient):
    path = "/api/v1/device/{uuid}"
    response_status_codes = [
        200,
    ]


class DeviceUpdate(BaseClient):
    path = "/api/v1/device/{uuid}/update"
    json_keys = [
        "slug",
        "name",
    ]
    response_status_codes = [
        200,
    ]


class DeviceCreate(BaseClient):
    path = "/api/v1/device/create"
    json_keys = [
        "slug",
        "name",
    ]
    response_status_codes = [
        200,
        201,
    ]


class DeviceDelete(BaseClient):
    path = "/api/v1/device/{uuid}/delete"
    response_status_codes = [
        200,
    ]


# Component

class ComponentUpdateValue(BaseClient):
    path = "/api/v1/device/{device_uuid}/component/{slug}/update/value"
    json_keys = [
        "value",
    ]
    response_status_codes = [
        200,
    ]


class ComponentUpdate(BaseClient):
    path = "/api/v1/device/{device_uuid}/component/{slug}/update"
    json_keys = [
        "name",
        "description",
    ]
    response_status_codes = [
        200,
    ]


class ComponentUpdateValue(BaseClient):
    path = "/api/v1/device/{device_uuid}/component/{slug}/update/value"
    json_keys = [
        "value",
    ]
    response_status_codes = [
        200,
    ]


class ComponentCreate(BaseClient):
    path = "/api/v1/device/{device_uuid}/component/create"
    json_keys = [
        "slug",
        "name",
        "description",
    ]
    response_status_codes = [
        200,
        201,
    ]


class ComponentDetailValue(BaseClient):
    path = "/api/v1/device/{device_uuid}/component/{slug}/value"
    response_status_codes = [
        200,
    ]


# Client

class MultiClient:
    child_clients = {}

    def __init__(self, host, api_key, **kwargs):
        self.host = host
        self.api_key = api_key
        self.kwargs = kwargs
        self.create_children()
        self.create()
        self.sync()

    def create(self):

        if self.kwargs.get("uuid"):
            return None

        create_client = getattr(self, "create_client", None)

        if create_client is None:
            raise NotImplementedError(
                f"{self.__class__} must implement .create() "
                f"or set an 'create_client' in .child_clients"
            )

        obj = self.create_client.post(**self.kwargs)["data"]
        self.kwargs["uuid"] = obj["uuid"]

    def sync(self):
        self.refresh_from_server()

    def refresh_from_server(self):
        self.server_state = self.get_server_state()

    def get_server_state(self):
        detail_client = getattr(self, "detail_client", None)

        if detail_client is None:
            raise NotImplementedError(
                f"{self.__class__} must implement .get_server_state() "
                f"or set an 'detail_client' in .child_clients"
            )

        return detail_client.get(**self.kwargs)["data"]

    def create_children(self):

        for key, client_class in self.get_child_clients().items():
            setattr(self, key, client_class(host=self.host, api_key=self.api_key))

    def get_child_clients(self):
        return self.child_clients.copy()


class Device(MultiClient):
    child_clients = {
        "detail_client": DeviceDetail,
        "update_client": DeviceUpdate,
        "create_client": DeviceCreate,
    }

    def sync(self):
        super().sync()

        components = getattr(self, "components", None)

        if components is None:
            self.components = self.create_components()

        for component in self.server_state["components"]:
            component = self.components[component["slug"]]
            component.sync()

    def create_components(self):
        return {
            component["slug"]: Component(
                device=self,
                uuid=component["uuid"],
                slug=component["slug"],
                name=component["name"],
                description=component["description"],
            )
            for component in self.server_state["components"]
        }

    def register_component(self, slug, changed_callback=None, **kwargs):
        component = self.components.get(slug)

        if component is None:
            component = Component(
                device=self,
                slug=slug,
                changed_callback=changed_callback,
                **kwargs,
            )
            self.components[slug] = component
        else:
            component.changed_callback = changed_callback

    def unregister_component(self, slug):
        component = self.components.pop(slug)
        component.delete()


class Component(MultiClient):
    child_clients = {
        "create_client": ComponentCreate,
        "update_client": ComponentUpdate,
        "update_value_client": ComponentUpdateValue,
    }
    
    def __init__(self,
                 device,
                 slug,
                 value="",
                 changed_callback=None,
                 **kwargs):
        self.device = device
        self.changed_callback = changed_callback
        super().__init__(
            host=self.device.host,
            api_key=self.device.api_key,
            device_uuid=self.device.kwargs["uuid"],
            slug=slug,
            value=value,
            **kwargs,
        )

    def create(self):
        super().create()
        self.device.refresh_from_server()

    def update(self, **kwargs):
        self.kwargs.update(kwargs)
        self.update_client.post(**self.kwargs)
        self.device.sync()

    def update_value(self, value):
        value = str(value)
        self.update_value_client.post(
            device_uuid=self.device.kwargs["uuid"],
            slug=self.kwargs["slug"],
            value=value,
        )
        self.kwargs["value"] = value
        self.device.sync()

    def sync(self):
        super().sync()

        server_val = self.server_state["value"]
        local_val = self.kwargs["value"]

        if local_val != server_val:
            self.kwargs["value"] = server_val

            if self.changed_callback is not None:
                self.changed_callback(self, local_val)

    def get_server_state(self):

        for component in self.device.server_state["components"]:
            
            if component["slug"] == self.kwargs["slug"]:
                return component


class IotClient:
    
    def __init__(self, host=None, api_key=None, loop=3, **kwargs):
        
        if host is None:
            host = os.getenv("IOT_HOST")

        assert host, (
            f"{self.__class__} must provide `host` or set the `IOT_HOST` "
            f"environment variable"
        )

        if api_key is None:
            api_key = os.getenv("IOT_API_KEY")

        assert api_key, (
            f"{self.__class__} must provide `api_key` or set the "
            f"`IOT_API_KEY` environment variable"
        )

        self.device = Device(host=host, api_key=api_key, **kwargs)

        self.loop_enabled = True
        self.loop_thread = threading.Thread(target=self.run, args=[loop])
        self.loop_thread.daemon = True
        self.loop_thread.start()

    def register_component(self, slug, **kwargs):
        self.device.register_component(slug, **kwargs)

    def unregister_component(self, slug):
        self.device.unregister_component(slug)

    def set_value(self, slug, value):
        self.loop_enabled = False
        self.device.components[slug].update_value(value=value)
        self.device.sync()
        self.loop_enabled = True

    def run(self, loop):
        
        while True:

            if self.loop_enabled:

                try:
                    self.device.sync()
                except Exception as e:
                    sys.stdout.write(traceback.format_exc() + "\n")

            time.sleep(loop)
